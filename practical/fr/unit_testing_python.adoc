﻿:source-highlighter: prettify
:source-highlighter: highlightjs

= TP : Build + Tests + Intégration continue
Régis WITZ
:doctype: book
:toc:
:toc-title:
:toclevels: 1

image::https://i.creativecommons.org/l/by-nc-nd/3.0/88x31.png[Creative Commons License, link="http://creativecommons.org/licenses/by-nc-nd/3.0/fr/"]


== Prérequis

Ce TP nécessite une machine comportant des installations fonctionnelles de https://www.python.org/[Python] (≥2.7), https://www.w3schools.com/python/python_pip.asp[pip] et https://tox.readthedocs.io/en/latest/[tox].

Il est possible que le logiciel `tox` ne soit pas installé sur votre machine de TP.
Dans ce cas, installez-le grâce au gestionnaire de paquets python `pip`.
Même si vous n'êts pas admin, vous pouvez entrer la commande suivante :
[source,bash]
----
pip install --user tox
----
Par défaut, `tox` sera ainsi installé dans le répertoire `$HOME/.local/bin`.
Vous pouvez donc invoquer tox grâce à la commande `~/.local/bin/tox`.
Pour pouvoir utiliser la commande `tox` directement, il vous faut ajouter `~/.local/bin` à votre variable d'environnement `PATH`, en entrant par exemple dans votre console ou votre fichier `~/.bashrc` la commande suivante :
[source,bash]
----
export PATH=$PATH:~/.local/bin
----



== Introduction: FizzBuzz

FizzBuzz est un problème d'algorithmique régulièrement posé lors des entretiens d'embauche.
Il n'est pas très compliqué en soi, et permet surtout aux recruteurs de ne pas perdre de temps avec des candidats aux compétences apparaissant trop limitées en informatique.

Une description courante de ce problème est la suivante:
[quote, 'https://en.wikipedia.org/wiki/Fizz_buzz[Wikipédia]']
____
Écrire un programme qui affiche les nombres de 1 à 100.
Cependant, pour les multiples de 3, affichez "Fizz" au lieu du nombre.
Pour les multiples de 5, affichez "Buzz" au lieu du nombre.
Pour les nombres qui sont à la fois multiples de 3 et de 5, affichez "FizzBuzz".
____

Par exemple, une réponse correcte à ce problème démarrerait de la manière suivante:
[source]
----
1, 2, Fizz, 4, Buzz, Fizz, 7, 8, Fizz, Buzz, 11, Fizz, 13, 14, FizzBuzz, 16, 17, Fizz, 19, Buzz, ...
----

Avant de vous précipiter pour trouver (facilement) la solution à ce problème sur Internet, je vous recommande de prendre un moment pour y réfléchir.
En effet, lors d'un entretien d'embauche, vous n'aurez peut-être que votre cerveau à votre disposition.
Alors, entraînez un peu vos méninges !

== Exercice 1: Configuration, build et tests unitaires

. Créez un répertoire de travail.
  Pour archiver ce que vous ferez dans ce TP et vous laisser la possibilité de revenir en arrière tout en révisant vos acquis du TP précédent sur la gestion de sources, je vous conseille de faire de ce répertoire un dépôt Git local et de commiter ce que vous avez fait à chaque étape de ce TP.

. Votre répertoire va devenir un projet Python.
  Pour cela, créez à la racine de votre répertoire un fichier `setup.py` similaire à celui-ci :
+
[source,python]
.setup.py
----
# -*- coding: utf-8 -*-
from setuptools import setup, find_packages

setup(
    name = 'fizzbuzz',
    version = '1.0.0',
    url = 'https://github.com/vous/fizzbuzz.git',
    author = 'Vous',
    author_email = 'vous@etu.unistra.fr',
    description = 'Réalisation du TP 02',
    packages = find_packages(),    
)
----
. Créez ensuite un fichier `tox.ini`.
  Ce fichier renferme la configuration de l'outil d'automatisation que nous allons utiliser : https://tox.readthedocs.io/en/latest/[tox].
+
[source,ini]
.tox.ini
----
# à mettre dans le même répertoire que votre setup.py
[tox]
# listez ici les environnements dans lesquels vous voulez tester votre projet
envlist = py27, py36

[testenv]
# listez toutes les dépendances de votre projet ici
# ici, nous n'avons besoin que d'une librairie de tests: pytest
deps = pytest
# listez les commandes à lancer
# ici, nous ne faisons qu'exécuter nos tests
commands = pytest
----

. Exécutez la commande `tox`.
  Lisez et comprenez la sortie de cette commande, ainsi que les fichiers et les dossiers qu'elle a générés.
  Si vous ne comprenez pas tout, appelez votre prof qui se fera un plaisir de vous expliquer.

. Vous avez donc compris qu'il y a plusieurs choses à corriger.
  D'abord, sont listés dans `tox.ini` deux environnements d'exécution : « python 2.7 », et « python 3.6 ».
  Or, il est probable que vous n'ayez pas exactement la même (ou les mêmes) versions de python installées. +
  En vérifiant éventuellement l'état de votre installation grâce à des commandes comme `ls -ls /usr/bin/python*` ou `python --version`, corrigez la propriété `envlist` de votre `tox.ini`. +
  Ensuite, relancez la commande `tox`. Qu'est-ce qui a changé ?

. Il vous faut ensuite créer le « squelette » de votre projet.
  Créez un répertoire `fizzbuzz`, puis créez-y le fichier `fizzbuzz.py` :
  (si vous avez changé le nom de votre projet dans votre `setup.py`, vous devriez changer le nom de ce répertoire en restant cohérent)
+
[source,python]
.fizzbuzz/fizzbuzz.py
----
# -*- coding: utf-8 -*-

class FizzBuzz:

    def convert(self, number):
        raise NotImplementedError
----

. Enfin, il vous faut créer un *module de tests unitaires* qui validera la qualité de votre projet.
  Créez le fichier `test_fizzbuzz.py` :
+
[source,python]
.fizzbuzz/test_fizzbuzz.py
----
# -*- coding: utf-8 -*-
from unittest import TestCase
from fizzbuzz import FizzBuzz

class FizzBuzzTest(TestCase):
    def test_returns_number_for_input_not_divisible_by_3_or_5(self):
        self.fizzbuzz = FizzBuzz()
        self.assertEqual('1',  self.fizzbuzz.convert(1))
        self.assertEqual('2',  self.fizzbuzz.convert(2))
        self.assertEqual('4',  self.fizzbuzz.convert(4))
        self.assertEqual('7',  self.fizzbuzz.convert(7))
        self.assertEqual('11', self.fizzbuzz.convert(11))
        self.assertEqual('13', self.fizzbuzz.convert(13))
        self.assertEqual('14', self.fizzbuzz.convert(14))
----

. Relancez `tox`.
  Remarquez que votre test unitaire, `test_returns_number_for_input_not_divisible_by_3_or_5`, est lancé.
  Cependant, ce test est en échec.
  Pourquoi ?

. Si vous implémentiez la classe `FizzBuzz` maintenant, l’unique méthode de test présente, `test_returns_number_for_input_not_divisible_by_3_or_5`, ne serait pas suffisante pour garantir la qualité de votre implémentation.
  En vous basant sur la spécification du problème FizzBuzz donnée dans l’introduction de ce TP, écrivez les méthodes de test manquantes. +
  Un indice ? +
  La spécification vous dit de faire quatre affichages différents, suivant le paramètre d'entrée.
  Actuellement, un seul de ces quatre cas est testé.
  Vous devriez donc créer au moins trois nouvelles méthodes de test.

. Implémentez la classe `FizzBuzz` proprement dite.
  Vous ne pourrez considérer votre implémentation correcte que lorsque la commande `tox` vous indiquera au moins 4 tests exécutés, avec 0 erreurs.
  Évidemment, vous n’avez le droit ni de supprimer, ni de commenter les tests, ni d’en passer l’exécution.

. Si vous utilisez `git` (et vous l'utilisez, hein ?), une fois que vous avez terminé l’implémentation et que tous les tests sont des succès, c'est le moment idéal pour commiter vos modifications.
  Attention, tout ce qui est présent dans votre dossier projet ne doit pas forcément être commité !

. Actuellement, puisque vos tests unitaires couvrent tout le périmètre fonctionnel, et sont des succès, la capacité fonctionnelle de votre projet est à priori garantie.
  Cependant, chacune des fonctionnalités du problème FizzBuzz n'est pas *testée en isolation*.
  Après avoir écouté votre prof vous expliquer en quoi cela consiste, *refactorez* votre code en retirant la ligne `self.fizzbuzz = FizzBuzz()` de chacune de vos fonction de test, et en rajoutant à votre classe de test une méthode d'initialisation `setUp` ainsi qu'une méthode de libération `tearDown`.
+
[source,python]
.fizzbuzz/test_fizzbuzz.py
----
# -*- coding: utf-8 -*-

# (imports)

class FizzBuzzTest(TestCase):

    def setUp(self):
        self.fizzbuzz = FizzBuzz()

    def tearDown(self):
        self.fizzbuzz = None

    # (vos 4 fonctions de test)
----



== Exercice 2: Couverture de code par les tests

. Dans chaque instruction peut se cacher un bug.
  Comment être sûr que nous testons effectivement _toutes_ nos lignes de code ? +
  Pour cela, nous allons tester la *couverture de code* de nos tests, grâce au module https://coverage.readthedocs.io/en/v4.5.x/[coverage]. +
  Modifiez votre fichier `tox.ini` de la manière suivante :
+
[source,ini]
.tox.ini
----

# ...

[testenv]
# on ajoute la dépendance envers coverage, ...
deps =
    pytest
    coverage
# ... et on modifie la commande d'exécution de nos tests comme suit :
commands =
    coverage erase
    coverage run -m pytest
    coverage report -m
----

. Relancez la commande `tox`.
  Pas facile d'y voir clair, hein ? +
  Pour configurer le module `coverage` de manière plus satisfaisante, créez le fichier `.coveragerc` :
+
[source,ini,title='.coveragerc']
----
[run]
source = fizzbuzz/
omit = setup.py
       .tox/*
----

. Relancez la commande `tox`.
  Alors, vous êtes à 100% ? ლ(｀ー´ლ)



== Exercice 3: Convention(s) d'écriture

. Votre code se doit d'être exact et entièrement testé.
  De plus, il se doit d'être _lisible_, et pour cela, le meilleur moyen est de respecter les *conventions d'écriture* de votre plateforme.
  En python, une telle convention est https://www.python.org/dev/peps/pep-0008/[PEP 8].
  Avez-vous, au cours de ce TP, écrit du « joli » Python ?
  C'est ce que nous allons découvrir.

. De la même manière qu'à l'exercice 2, rajoutez une nouvelle dépendance à votre projet : `flake8`.
  Puis, rajoutez la section suivante à la fin de votre fichier `tox.ini`.
+
[source,ini]
.tox.ini
----
[testenv:flake8]
commands=
    flake8 --count --show-source
----
+
Notez qu'en créant une nouvelle section ainsi, `flake8` doit aussi être rajouté à votre `envlist`.
Comparez cette manière de faire avec celle via laquelle vous avez utilisé `coverage` dans l'exercice précédent.

. Relancez la commande `tox`.
  Corrigez votre syntaxe jusqu'à ce que `flake8` ne trouve plus rien à redire à votre code.
  Bravo, vous pouvez être fier(e) de vous ! (•̀ᴗ•́)و ̑̑



== Exercice 4: Mocks d'objets pour tester en isolation

À ce point du TP, la classe `FizzBuzz` est fonctionnelle et correctement testée.
Cependant, elle ne répond pas totalement au problème initial, qui spécifie d'_afficher_ les réponses correspondant aux nombres de 1 à 100.
Il est donc nécessaire d'implémenter, et donc de tester, une classe utilitaire `ProblemSolver` qui permettra d'achever le travail.

image::TP-02.4.1.png[caption="", title="`ProblemSolver` ne connaît que les interfaces `Int2String` et `Displayer` ; il n'en connaît aucune implémentation concrète, et en particulier pas `FizzBuzz`."]

. Créez un nouveau module, `solver`, déclarant les classes abstraites `Int2String` et `Displayer`, ainsi que la classe concrète `ProblemSolver`.

. Créez un nouveau module de tests `test_problemsolver.py`.
  Ce module devra permettre de tester un objet de type `ProblemSolver` _SANS_ lui donner en arguments d'objets réellement instanciés.
  À la place, vous devrez lui donner des mocks instanciés grâce au module https://docs.python.org/3/library/unittest.mock-examples.html[unittest.mock].
  Pour vous aider, voici le « squelette » du module `test_problemsolver.py`:
+
[source,python]
.fizzbuzz/test_problemsolver.py
----
# -*- coding: utf-8 -*-
from unittest import TestCase
from unittest.mock import patch
from solver import ProblemSolver

# ... définition des fonctions mock_convert et mock_display

class ProblemSolverTest(TestCase):

    def setUp(self):
        with patch('solver.Int2String') as mock:
            mock.convert = mock_convert
            converter = mock
        with patch('solver.Displayer') as mock:
            mock.display = mock_display
            displayer = mock
        self.solver = ProblemSolver(converter, displayer)

# ... méthodes testant le retour d'un appel à self.solver.solve
----

== Exercice 5: Intégration continue

Cet exercice est relativement technique.
Le prochain TP vous permettra de faire quelque chose de relativement similaire, mais dans un cas plus simple que celui-ci.
Il peut donc être profitable pour vous de revenir à cet exercice après avoir bouclé le prochain TP.

Le but est d'apprendre à configurer un *serveur d'intégration continue* grâce à https://github.com/[GitHub] et un de ses services tiers : https://travis-ci.org/[Travis CI].
Si ça n'est pas déjà fait, n'hésitez pas à prendre le temps de vous familiariser avec leurs interfaces, leurs possibilités mais aussi leurs limitations.

Si vous avez, comme je l'ai conseillé, utilisé un dépôt Git local, vous pouvez le faire pointer sur un dépôt GitHub distant, puis le pusher.

Ensuite, il vous faudra réorganiser les fichiers de votre projet en accord avec les https://docs.pytest.org/en/latest/goodpractices.html[bonnes pratiques] de `setuptools`, `pytest` et `tox`.

Enfin, il vous faudra créer le fichier `.travis.yml` à la racine de votre dépôt, afin de dire à Travis CI comment builder votre projet.
Voici un exemple pour débuter :
[source]
.travis.yml
----
# Vous n'aurez à priori pas besoin des droits d'administration pour cet exo.
# Cependant, cela peut être utile si, par exemple, vous devez installer sur
# votre serveur d'intégration continue des paquets supplémentaires avec apt.
# Pas d'inquiétude, les serveurs d'intégration continue proposés par des
# services comme Travis CI sont en fait des machines virtuelles qui n'existent
# que le temps de construire votre application. Donc, si vous "cassez" quelque
# chose avec vos droits admin, ça ne gênera que votre build, et aucun autre
# utilisateur du service. Vous n'aurez qu'à corriger votre configuration et
# pusher vos modifications pour retenter votre chance avec une VM neuve !
sudo: false

# Votre projet est en python, il vous faut donc du python sur votre serveur.
language: python
# Grâce à un serveur d'intégration continue, vous pouvez builder votre projet
# dans différents environnements, même si ceux-ci ne sont pas présent sur
# votre machine de développement.
# Par exemple, vous pouvez demander à ce que votre serveur ait les 3 versions
# de python suivantes directement installer et, en modifiant tox.ini comme ça ...
#   envlist = py{27,35,36},flake8
# ... lancer 3 builds différents dans 3 environnements python différents, afin 
# que votre code soit compatible avec un maximum d'utilisateurs différents.
python:
  - "2.7"
  - "3.5"
  - "3.6"

# Avant de builder, Travis CI vous laisse la possibilité de configurer/installer
# des composants supplémentaires sur votre serveur d'intégration continue.
# Pour cet exercice, il vous faudra au minimum installer tox.
install: pip install tox

# Cette dernière directive décrit les commandes à lancer pour builder.
# Puisque tous les détails sont gérés dans votre tox.ini, vous pouvez vous
# contenter de lancer seulement tox.
# C'est l'avantage d'utiliser un tel outil.
# Si vous voulez tout builder à la main (sans tox ou autre outil similaire),
# il vous faut mettre vos différentes instructions ici
# (ou éventuellement écrire un script dédié).
script: tox
----

Vous pouvez aussi faire plein d'autres choses intéressantes grâce à ce système.
Par exemple, vous pouvez générer la documentation de vos classes grâce à un outil comme https://pdoc3.github.io/pdoc/[pdoc].
Ensuite, vous pouvez héberger cette doc directement en ligne grâce aux https://pages.github.com/[GitHub Pages].

À noter que si vous voulez builder des projets pour des systèmes Windows, cela n'est pas possible avec Travis CI.
Pour ce faire, vous pouvez faire appel à un service spécialisé, comme https://www.appveyor.com/[AppVeyor].
La configuration s'y fait de manière quasiment similaire à Travis CI.

== Références

* https://docs.python.org/3/library/unittest.mock.html[Documentation] du module python `unittest`.
* https://setuptools.readthedocs.io/en/latest/[Python Setuptools] (c'est de là que vient votre `setup.py`).
* https://tox.readthedocs.io/en/latest/examples.html[tox: configuration et exemples].
* Une proposition de numérotation de version à adopter pour vos projets: https://semver.org/[semantic versioning].
* https://coverage.readthedocs.io/en/v4.5.x/[Documentation de coverage].
* La convention d'écriture https://www.python.org/dev/peps/pep-0008/[PEP 8], ainsi qu'une petite explication de https://medium.com/python-pandemonium/what-is-flake8-and-why-we-should-use-it-b89bd78073f2[pourquoi c'est important].
* Comment faire des https://docs.python.org/3/library/unittest.mock-examples.html[mocks].
* Description de l'API Python https://docs.python.org/3/library/abc.html[abc (Abstract Base Classes)].
  Et aussi un https://www.python-course.eu/python3_abstract_classes.php[petit tutoriel], tant qu'à faire.
